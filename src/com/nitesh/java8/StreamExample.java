package com.nitesh.java8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamExample {

	public static void main(String[] args) {

		List<Integer> myList = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			myList.add(i);
		}

		//sequential stream
		Stream<Integer> sequentialStream = myList.stream();

		//parallel stream
		Stream<Integer> parallelStream = myList.parallelStream();

		//using lambda with Stream API, filter example
		Stream<Integer> highNums = parallelStream.filter(p -> p > 90);
		//using lambda in forEach
		highNums.forEach(p -> System.out.println("High Nums parallel= ," + Thread.currentThread().getName() + " " + p));

		Stream<Integer> highNumsSeq = sequentialStream.filter(p -> p > 90);
		highNumsSeq.forEach(p -> System.out.println("High Nums sequential=" + p));

		// We can use java Stream collect() method to get List, Map or Set from stream.
		final List<Integer> collectList = myList.stream().filter(integer -> integer % 10 == 0).collect(Collectors.toList());
		System.out.println("collectList: " + collectList);

		// We can use java Stream collect() method to get Map or Set from stream.
		final Map<Integer, Integer> collectMap = myList.stream().filter(integer -> integer % 10 == 0).collect(Collectors.toMap(p -> p, p -> p * p));
		System.out.println("collectMap: " + collectMap);

		//We can use stream toArray() method to create an array from the stream.
		final Integer[] intArray = myList.stream().toArray(Integer[]::new);

		final List<String> names = Arrays.asList("aBc", "lmn", "d", "ef", "123456");
		System.out.println(names.stream().map(s -> s.toUpperCase()).collect(Collectors.toList()));

		System.out.println(names.stream().sorted().collect(Collectors.toList()));

		System.out.println(names.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()));

		final List<Integer> numbers = Arrays.asList(1, 12,43,53,5,12,45);
		System.out.println(numbers.stream().reduce(Integer::compareTo));
	}
}

